'use strict';

describe('myApp.view1 module', function() {

  beforeEach(module('myApp.view1'));

  describe('view1 controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      let view1Ctrl = $controller('View1Ctrl');
      expect(view1Ctrl).toBeDefined();
    }));

    it('getUserById should work', inject(function($controller) {
      let view1Ctrl = $controller('View1Ctrl');
      let alysson = view1Ctrl.getUserById(1);
      expect(alysson.name).toBe('Alysson');

      let andre = view1Ctrl.getUserById(3);
      expect(andre).toBeUndefined();
    }));

  });
});
