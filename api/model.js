class Model {
  constructor (conn) {
    this._maxLimit = 999;
    this._offset = 0;
    this._page = 1;
    this._limit = 10;

    this._conn = conn;
  }

  conn () {
    return this._conn;
  }

  limit (_limit) {
    if (_limit === void 0) {
      return this._limit;
    }
    if (_limit > 0 && _limit > this._maxLimit) {
      throw new Error('Limit não pode execer o limíte máximo do componente');
    }

    this._limit = _limit;
    return this;
  }

  offset (_offset) {
    if (_offset === void 0) {
      return this._offset;
    }
    if (_offset < 0) {
      throw new Error('Offset deve ser no mínimo 0');
    }

    this._offset = _offset;
    return this;
  }

  page (_page) {
    if (_page === void 0) {
      return this._page;
    }
    if (_page < 1) {
      throw new Error('Page deve ser no mínimo 1');
    }

    this._page = _page;
    return this;
  }

  paginateStmt (_lastStmt) {
    let countStmt = 'SELECT count(*) items_count, (count(*) / 10) pages_count, ' +
      `${this._page} page,  ${this._limit} "limit", ${this._offset} "offset" FROM`;
    countStmt = _lastStmt.toUpperCase()
      .replace(/SELECT(.*?)FROM/, countStmt)
      .replace(/LIMIT([\s\d]+)OFFSET([\s\d]+)/, '');

    return countStmt;
  }
}

module.exports = Model;
