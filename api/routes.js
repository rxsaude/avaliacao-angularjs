const express = require('express');
const sqlite = require('sqlite');
const Promise = require('bluebird');

const routes = express.Router();
const dbPromise = sqlite.open('./api/dataset.sqlite', { Promise });
const PartidosModel = require('./partidos');

/**
 * GET root endpoint
 */
routes.route('/')
  .get((req, res, next) => {
    res.json({
      status: 200,
      message: 'Everything is ok'
    });
  });

routes.route('/partidos')
  .get(async (req, res, next) => {
    try {
      const filter = req.query.filter;
      const limit = req.query.limit || 10;
      let offset = 0;
      let page = 1;

      if (req.query.offset >= 0) {
        offset = req.query.offset;
        page = (offset / limit) + 1;
      } else if (req.query.page > 0) {
        page = req.query.page;
        offset = (page - 1) * limit;
      }

      const partidos = (new PartidosModel(dbPromise))
        .limit(limit)
        .offset(offset)
        .page(page);

      const response = await partidos.all(filter);
      response.status = 200;

      res.json(response);
    } catch (err) {
      next(err);
    }
  });

routes.route('/partidos/:partido_id')
  .get(async (req, res, next) => {
    try {
      const partidos = (new PartidosModel(dbPromise));
      const response = await partidos.view(req.params.partido_id);
      response.status = 200;

      res.json(response);
    } catch (err) {
      next(err);
    }
  });
// const routes = require('./routes');

routes.route('/deputados')
  .get(async (req, res, next) => {
    try {
      const db = await dbPromise;
      const [data, pagination] = await Promise.all([
        db.all('SELECT id, nome, sigla, uf, ano_legislatura, carteira_parlamentar FROM deputados ORDER BY sigla LIMIT 10 OFFSET 0'),
        db.get('SELECT 0 start, 10 size, count(*) count, count(*) / 10 pages FROM deputados'),
      ]);
      res.json({
        status: 200,
        data: data,
        pagination: pagination
      });
    } catch (err) {
      next(err);
    }
  });

routes.route('/gastos')
  .get(async (req, res, next) => {
    try {
      const db = await dbPromise;
      const [data, pagination] = await Promise.all([
        db.all('SELECT * FROM gastos ORDER BY sigla, id_deputado LIMIT 10 OFFSET 0'),
        db.get('SELECT 0 start, 10 size, count(*) count, count(*) / 10 pages FROM gastos'),
      ]);
      res.json({
        status: 200,
        data: data,
        pagination: pagination
      });
    } catch (err) {
      next(err);
    }
  });


module.exports = routes;
