const Promise = require('bluebird');
const Model = require('./model');

class PartidosModel extends Model {
  queryAllStmt (where) {
    if (!where) {
      where = true;
    }
    const offset = this.offset();
    const limit = this.limit();
    return 'SELECT sigla, nome, codigo_eleitoral, qtd_filiados, criado_em, registrado_em ' +
      `FROM partidos WHERE ${where} ORDER BY sigla LIMIT ${limit} OFFSET ${offset}`;
  }

  async all (filter) {
    let whereStmt, whereVal;
    if (filter) {
      whereVal = filter + '%';
      whereStmt = 'sigla like ?';
    }
    const db = await this.conn();
    const dataStmt = this.queryAllStmt(whereStmt);
    const paginationStmt = this.paginateStmt(dataStmt);

    const [data, pagination] = await Promise.all([
      db.all(dataStmt, whereVal),
      db.get(paginationStmt),
    ]);

    return {
      data: data,
      pagination: pagination,
    };
  }

  queryViewStmt () {
    const offset = this.offset();
    const limit = this.limit();
    return 'SELECT sigla, nome, codigo_eleitoral, qtd_filiados, criado_em, registrado_em ' +
      `FROM partidos ORDER BY sigla LIMIT ${limit} OFFSET ${offset}`;
  }

  async view (sigla) {
    const db = await this.conn();
    const dataStmt = 'SELECT sigla, nome, codigo_eleitoral, qtd_filiados, criado_em, registrado_em FROM partidos WHERE sigla = ?';
    const metricsStmt = 'WITH partido (sigla) AS (VALUES (?)),\n' +
      '     top_gastos (sigla, descricao, total) AS (\n' +
      '         SELECT g.sigla, descricao, round(sum(valor), 2) total\n' +
      '         FROM gastos g\n' +
      '         JOIN partido p ON (g.sigla = p.sigla)\n' +
      '         GROUP BY g.sigla, g.descricao\n' +
      '         ORDER BY total desc\n' +
      '         LIMIT 4\n' +
      '     ),\n' +
      '     outros_gastos(sigla, descricao, total) AS (\n' +
      '         SELECT g.sigla, \'Outros\' descricao, round(sum(valor), 2) total\n' +
      '         FROM gastos g\n' +
      '         JOIN partido p ON (g.sigla = p.sigla)\n' +
      '         LEFT JOIN top_gastos tg ON (g.sigla = tg.sigla AND g.descricao = tg.descricao)\n' +
      '         WHERE tg.descricao is null\n' +
      '         GROUP BY g.sigla\n' +
      '         ORDER BY total\n' +
      '     )\n' +
      'SELECT * FROM top_gastos\n' +
      'UNION\n' +
      'SELECT * FROM outros_gastos\n' +
      'ORDER BY total desc';

    const [data, metrics] = await Promise.all([
      db.get(dataStmt, sigla),
      db.all(metricsStmt, sigla),
    ]);
    data.metrics = metrics;

    return {
      data: data,
    };
  }
}

module.exports = PartidosModel;
