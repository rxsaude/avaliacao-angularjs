const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');

const app = express();
const port = process.env.PORT || 8000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
app.use(function (err, req, res, next) {
  console.log(err);
  res.status(err.status || 500);
  res.json({
    status: err.status,
    error: err.message
  });
});

app.listen(port, () => {
  console.log(`API running on port ${port}`);
});

module.exports = app;
