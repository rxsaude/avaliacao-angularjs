import angular from 'angular';
import 'bootstrap/scss/bootstrap.scss';
import './sass/app.scss';
import { LayoutModule } from './layout';
import { RoutesModule } from './routes';

export const rxAppComponent = angular
  .module('rxApp', [
    LayoutModule,
    RoutesModule,
  ])
  .name;
