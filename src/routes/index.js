import angular from 'angular';
import uiRouter from 'angular-ui-router';

import { PartiesListComponent } from './parties/list.component';
import { PartyViewComponent } from './parties/view.component';
import { PartiesFactory } from './parties.service';

RoutingConfig.$inject = ['$urlRouterProvider', '$locationProvider'];
export function RoutingConfig ($urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(false);
  // $locationProvider.hashPrefix('');
  $urlRouterProvider.otherwise('/partidos');
}

export const RoutesModule = angular
  .module('rxApp.routes', [
    uiRouter,
    PartiesFactory,
    PartiesListComponent,
    PartyViewComponent,
  ])
  .config(RoutingConfig)
  .name;
