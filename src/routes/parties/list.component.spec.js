import angular from 'angular';
import 'angular-mocks/angular-mocks';
import ngResource from 'angular-resource';
import { PartiesFactory, PartiesFactoryTag } from '../parties.service';
import { PartiesListComponent, PartiesListTag } from './list.component';

describe('Component ' + PartiesListTag, () => {
  let $componentController;
  let $injector;

  beforeEach(angular.mock.module(ngResource));
  beforeEach(angular.mock.module(PartiesFactory));
  beforeEach(angular.mock.module(PartiesListComponent));
  beforeEach(angular.mock.inject((_$injector_) => {
    $injector = _$injector_;
  }));
  beforeEach(angular.mock.inject((_$componentController_) => {
    $componentController = _$componentController_;
  }));

  it('should be defined', function () {
    const component = $componentController(PartiesListTag, {}, {});
    expect(component).toBeDefined();
  });

  it('should have scope', function () {
    const scope = $injector.get('$rootScope');
    scope.test = 123;

    const component = $componentController(PartiesListTag, scope, {});
    expect(component.scope.test).toBe(scope.test);
  });
});
