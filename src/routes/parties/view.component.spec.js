import angular from 'angular';
import 'angular-mocks/angular-mocks';
import ngResource from 'angular-resource';
import { PartiesFactory } from '../parties.service';
import { PartyViewComponent, PartyViewTag } from './view.component';

describe('Component ' + PartyViewTag, () => {
  let $componentController;

  beforeEach(angular.mock.module(ngResource));
  beforeEach(angular.mock.module(PartiesFactory));
  beforeEach(angular.mock.module(PartyViewComponent));
  beforeEach(angular.mock.inject((_$componentController_) => {
    $componentController = _$componentController_;
  }));

  it('should be defined', function () {
    const component = $componentController(PartyViewTag, {}, {});
    expect(component).toBeDefined();
  });

  it('should be working', function () {
    const component = $componentController(PartyViewTag, {}, {});
    expect(component.isWorking).toBe('yes');
  });
});
