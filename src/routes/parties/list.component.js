import angular from 'angular';
import uiRouter from 'angular-ui-router';
import './list.component.scss';

PartiesRoutes.$inject = ['$stateProvider'];
function PartiesRoutes ($stateProvider) {
  $stateProvider
    .state('parties-list', {
      url: '/partidos',
      component: PartiesListTag
    });
}

class PartiesListController {
  constructor ($rootScope, PartiesFactory) {
    this.Model = PartiesFactory;

    this.scope = $rootScope;
    this.isLoading = true;

    this.Model.query({ filter: '' }).$promise
      .then(response => {
        this.parties = response.data;
      })
      .then(() => {
        this.isLoading = false;
      });
  }

  getParties (searchInput) {
    this.isLoading = true;

    return this.parties;
  }

  $onInit () {
    const self = this;

    this.scope.$watch('search.input', function (newValue, oldValue) {
      self.getParties(newValue);
    });
  }
}

export const PartiesListTag = 'rxPartiesList';
export const PartiesListComponent = angular
  .module('rxApp.routes.parties-list', [
    uiRouter,
  ])
  .component(PartiesListTag, {
    controller: PartiesListController,
    template: require('./list.component.html').default,
  })
  .config(PartiesRoutes)
  .name;
