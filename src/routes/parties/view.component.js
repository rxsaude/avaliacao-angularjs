import angular from 'angular';
import './view.component.scss';
import 'angular-chart.js';

class PartyViewController {
  constructor (PartiesFactory) {
    this.Model = PartiesFactory;

    this.isWorking = 'yes';
    this.isLoading = true;

    this.series = ['Gastos'];
    this.labels = [];
    this.data = [];
    this.options = {
      scales: {
        yAxes: [
          {
            id: 'y-axis-1',
            display: false,
            type: 'linear',
            gridLines: {
              display: false
            }
          }
        ],
        xAxes: [
          {
            id: 'x-axis-1',
            display: false,
            gridLines: {
              display: false
            }
          }
        ]
      }
    };
  }

  $onInit () {
    this.Model.get(this.view.sigla).$promise
      .then(response => this.mapViewResults(response.data))
      .then(() => {
        this.isLoading = false;
      });
  }

  mapViewResults (response) {
    this.isLoading = true;
    this.labels = response.metrics.map(item => item.descricao);
    this.data = response.metrics.map(item => item.total);

    return response;
  }
}

export const PartyViewTag = 'rxPartyView';
export const PartyViewComponent = angular
  .module('rxApp.routes.party-view', ['chart.js'])
  .component(PartyViewTag, {
    bindings: {
      view: '<',
    },
    template: require('./view.component.html').default,
    controller: PartyViewController,
    controllerAs: '$ctrl'
  })
  .name;
