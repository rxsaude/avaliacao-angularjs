import angular from 'angular';
import { MainComponent } from './main.component';

export const LayoutModule = angular
  .module('rxApp.layout', [
    MainComponent,
  ])
  .name;
