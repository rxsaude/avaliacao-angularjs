import angular from 'angular';
import 'angular-mocks/angular-mocks';
import { MainComponent, MainTag } from './main.component';

describe('Component ' + MainTag, function () {
  let $componentController;

  beforeEach(angular.mock.module(MainComponent));
  beforeEach(angular.mock.inject(function (_$componentController_) {
    $componentController = _$componentController_;
  }));

  it('should be defined', function () {
    const component = $componentController(MainTag, {}, {});
    expect(component).toBeDefined();
  });

  it('should be working', function () {
    const component = $componentController(MainTag, {}, {});
    expect(component.isWorking).toBe('yes');
  });

  it('should be searching', function () {
    const component = $componentController(MainTag, {}, {});
    expect(component.search.input).toBeDefined();
  });
});
