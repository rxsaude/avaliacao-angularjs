import angular from 'angular';
import 'angular-mocks/angular-mocks';
import { HeaderComponent, HeaderTag } from './header.component';

describe('Component ' + HeaderTag, () => {
  let $componentController;

  beforeEach(angular.mock.module(HeaderComponent));
  beforeEach(angular.mock.inject((_$componentController_) => {
    $componentController = _$componentController_;
  }));

  it('should be defined', function () {
    const component = $componentController(HeaderTag, {}, {});
    expect(component).toBeDefined();
  });

  it('should be working', function () {
    const component = $componentController(HeaderTag, {}, {});
    expect(component.isWorking).toBe('yes');
  });
});
