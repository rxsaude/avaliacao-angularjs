import angular from 'angular';
import './header.component.scss';

class HeaderController {
  constructor () {
    this.isWorking = 'yes';
  }
}

export const HeaderTag = 'rxHeader';
export const HeaderComponent = angular.module('rxApp.layout.header', [])
  .component(HeaderTag, {
    bindings: {
      searchInput: '=',
    },
    controller: HeaderController,
    controllerAs: '$ctrl',
    template: require('./header.component.html').default,
  })
  .name;
