// jshint strict: false

// Reference: http://karma-runner.github.io/0.12/config/configuration-file.html
module.exports = (config) => {
  config.set({
    // ... normal karma configuration
    files: [
      'src/tests.webpack.js',
      // { pattern: 'src/*.spec.js', watched: false },
      // { pattern: 'src/**/*.spec.js', watched: false },
    ],

    preprocessors: {
      // Reference: http://webpack.github.io/docs/testing.html
      // Reference: https://github.com/webpack/karma-webpack
      // Convert files with webpack and load sourcemaps
      'src/tests.webpack.js': ['webpack', 'sourcemap'],
      // 'src/**/*.js': ['coverage'],
      // 'src/*.spec.js': ['webpack', 'sourcemap'],
      // 'src/**/*.spec.js': ['webpack', 'sourcemap'],
    },

    reporters: [
      // Reference: https://github.com/mlex/karma-spec-reporter
      // Set reporter to print detailed results to console
      'spec',

      // Reference: https://github.com/karma-runner/karma-coverage
      // Output code coverage files
      'coverage'
    ],

    frameworks: [
      // Reference: https://github.com/karma-runner/karma-jasmine
      // Set framework to jasmine
      'jasmine'
    ],

    webpack: require('./webpack.config'),

    webpackMiddleware: {
      // webpack-dev-middleware configuration
      // i. e.
      noInfo: 'errors-only',
    },

    // singleRun: true,

    // Configure code coverage reporter
    coverageReporter: {
      dir: 'coverage/',
      reporters: [
        { type: 'text-summary' },
        { type: 'html' }
      ]
    },

    specReporter: {
      maxLogLines: 5, // limit number of lines logged per test
      suppressErrorSummary: true, // do not print error summary
      suppressFailed: false, // do not print information about failed tests
      suppressPassed: false, // do not print information about passed tests
      suppressSkipped: true, // do not print information about skipped tests
      showSpecTiming: true, // print the time elapsed for each spec
      failFast: false // test would finish with error when a first fail occurs.
    },

    browsers: [
      // Run tests using PhantomJS
      'PhantomJS',
      // 'Chrome',
    ],

    // client: {
    //   captureConsole: false,
    // },

    plugins: [
      'karma-webpack',
      'karma-coverage',
      'karma-spec-reporter',
      'karma-sourcemap-loader',
      'karma-chrome-launcher',
      'karma-phantomjs-launcher',
      'karma-jasmine',
    ]
  });
};
